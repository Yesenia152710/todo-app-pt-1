import React, { Component } from "react";
import todoList from "./components/todos.json";
import "./App.css";

class App extends Component {
  state = {
    todo: todoList,
    newTask: "",
  };

  addTask = (event) => {
    if (event.keyCode === 13) {
      const newItems = this.state.todo;
      const newItem = {
        userId: 1,
        id: Math.random(),
        title: this.state.newTask,
        completed: false,
      };
      newItems.push(newItem);
      this.setState({ todo: newItems, newTask: "" });
    }
  };

  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };

  toggleDone = (todoId) => (event) => {
    const newItems = this.state.todo;

    newItems.forEach((todo) => {
      if (todo.id === todoId) {
        todo.completed = !todo.completed;
      }
    });
    this.setState({ todo: newItems });
  };

  handleClear = (todoId) => (event) => {
    const newItems = this.state.todo.filter((todo) => todo.id !== todoId);

    this.setState({ todo: newItems });
  };

  clearAll = () => {
    const newItems = this.state.todo.filter((todo) => todo.completed === false);

    this.setState({ todo: newItems });
  };

  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>Todos</h1>
          <input
            className="new-todo"
            name="newTask"
            value={this.state.newTask}
            onChange={this.handleChange}
            onKeyDown={this.addTask}
            placeholder="Add to Todo list"
            autoFocus
          />
        </header>
        <TodoList todo={this.state.todo} toggleDone={this.toggleDone} handleClear={this.handleClear} />
        <footer className="footer">
          <span className="todo-count">
            <strong>0</strong> item(s) left
          </span>
          <button className="clear-completed" onClick={this.clearAll}>
            Clear completed
          </button>
        </footer>
      </section>
    );
  }
}

class TodoItem extends Component {
  render() {
    return (
      <li className={this.props.completed ? "completed" : ""}>
        <div className="view">
          <input className="toggle" type="checkbox" checked={this.props.completed} onClick={this.props.toggleDone} />
          <label>{this.props.title}</label>
          <button className="destroy" onClick={this.props.handleClear} />
        </div>
      </li>
    );
  }
}

class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todo.map((todo) => (
            <TodoItem
              title={todo.title}
              completed={todo.completed}
              toggleDone={this.props.toggleDone(todo.id)}
              handleClear={this.props.handleClear(todo.id)}
            />
          ))}
        </ul>
      </section>
    );
  }
}

export default App;
